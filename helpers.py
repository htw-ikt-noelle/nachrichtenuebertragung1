import schemdraw, os, sys
from schemdraw import dsp
import numpy as np
import scipy.signal as ssignal
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath('./skcomm'))
import skcomm as skc

def gen_block_diagramm():    
    annColor = '#00B9FF'    
    nue1Color = '#AAFFAA'
    nue2Color = '#FFAAAA'

    # schemdraw.config(bgcolor=None)

    with schemdraw.Drawing(show=False) as d:
        d.config(fontsize=12)
        # bit source
        d += (ds := dsp.Box(w=2,h=2)).label('Digitale \n Daten-\nquelle').fill(nue1Color)
        d += (l1 := schemdraw.elements.lines.Line()).at(ds.E).right().length(1)
        d += (a1 := schemdraw.elements.lines.Annotate()).at(l1.center).to((l1.center[0],l1.center[1]-1)).color(annColor).label('Bits')
        # source coding
        d += (sc := dsp.Box(w=2,h=2)).label('Quellen-\ncodierung').at(l1.end).linestyle('--').fill(nue1Color)
        d += (l2 := schemdraw.elements.lines.Line()).at(sc.E).right().length(1)
        d += (a2 := schemdraw.elements.lines.Annotate()).at(l2.center).to((l2.center[0],l2.center[1]-1)).color(annColor).label('Bits')
        # channel coding
        d += (cc := dsp.Box(w=2,h=2)).label('Kanal-\ncodierung').at(l2.end).linestyle('--').fill(nue2Color)
        d += (l3 := schemdraw.elements.lines.Line()).at(cc.E).right().length(1)
        d += (a3 := schemdraw.elements.lines.Annotate()).at(l3.center).to((l3.center[0],l3.center[1]-1)).color(annColor).label('Bits')
        # line coding
        d += (lc := dsp.Box(w=2,h=2)).label('Leitungs-\ncodierung').at(l3.end).linestyle('--').fill(nue1Color)
        d += (l4 := schemdraw.elements.lines.Line()).at(lc.E).right().length(1)
        d += (a4 := schemdraw.elements.lines.Annotate()).at(l4.center).to((l4.center[0],l4.center[1]-1)).color(annColor).label('Bits')
        # mapper
        d += (ma := dsp.Ic(pins=[dsp.IcPin(side='L'), dsp.IcPin(side='R'), dsp.IcPin(side='R')],
                            size=(2,2), leadlen=0.0).anchor('inL1').label('Mapper')).at(l4.end).fill(nue1Color)
        d += (l5 := schemdraw.elements.lines.Line()).at(ma.inR1).right().length(1).linestyle('--')
        d += (l6 := schemdraw.elements.lines.Line()).at(ma.inR2).right().length(1)
        d += (a5 := schemdraw.elements.lines.Annotate()).at(l5.center).to((l5.center[0],l5.center[1]-1)).color(annColor).label('(komplexe)\n digitale \n Symbole / Bits')
        # pulseforming
        d += (pf := dsp.Ic(pins=[dsp.IcPin(side='L'), dsp.IcPin(side='L'), dsp.IcPin(side='R'), dsp.IcPin(side='R')],
                            size=(2,2), leadlen=0.0).anchor('inL1').label('Puls-\nformung / \n dsp')).at(l5.end).fill(nue1Color)
        d += (l7 := schemdraw.elements.lines.Line()).at(pf.inR1).right().length(1).linestyle('--')
        d += (l8 := schemdraw.elements.lines.Line()).at(pf.inR2).right().length(1)
        d += (a6 := schemdraw.elements.lines.Annotate()).at(l7.center).to((l7.center[0],l7.center[1]-1)).color(annColor).label('(komplexes)\n digitales \n Signal')
        # DAC
        d += (dac := dsp.Ic(pins=[dsp.IcPin(side='L'), dsp.IcPin(side='L'), dsp.IcPin(side='R'), dsp.IcPin(side='R'),dsp.IcPin(side='T')],
                            size=(2,2), leadlen=0.0).anchor('inL1').label('Digital-\nAnalog-\nKonverter')).at(l7.end).fill(nue1Color)
        d += (l9 := schemdraw.elements.lines.Line()).at(dac.inR1).right().length(1).linestyle('--')
        d += (l10 := schemdraw.elements.lines.Line()).at(dac.inR2).right().length(1)
        d += (a7 := schemdraw.elements.lines.Annotate()).at(l9.center).to((l9.center[0],l9.center[1]-1)).color(annColor).label('(komplexes)\n analoges \n Signal')
        # Modulator
        d += (mod := dsp.Ic(pins=[dsp.IcPin(side='L'), dsp.IcPin(side='L'), dsp.IcPin(side='R'),dsp.IcPin(side='T')],
                            size=(2,2), leadlen=0.0).anchor('inL1').label('Modulator')).at(l9.end).linestyle('--').fill(nue2Color)
        d += (l11 := schemdraw.elements.lines.Line()).at(mod.inR1).right().length(1)
        d += (l12 := schemdraw.elements.lines.Line()).at(l11.end).down().length(2)    
        d += (arr1 := schemdraw.elements.lines.Arc2(arrow='<->',k=0.5)).to(mod.inT1).at(dac.inT1).linestyle('--')
        # Kanal + Filter
        d += (ch := dsp.Box(w=2,h=2)).label('Kanal').at(l12.end).fill(nue1Color)
        d += (l13 := schemdraw.elements.lines.Line()).at(ch.S).down().length(1)
        d += (a9 := schemdraw.elements.lines.Annotate()).at(l13.center).to((l13.center[0]-4,l13.center[1])).color('#00B9FF').label('')
        d += (filt := dsp.Box(w=2,h=2)).label('Filter').at(l13.end).linestyle('--').fill(nue1Color)
        d += (l14 := schemdraw.elements.lines.Line()).at(filt.S).down().length(2)
        d += (l15 := schemdraw.elements.lines.Line()).at(l14.end).left().length(1)
        d += (a8 := schemdraw.elements.lines.Annotate()).at(l11.center).to((l13.center[0]-4,l13.center[1])).color('#00B9FF').label('reelles analoges Signal\n (entweder Basisband- oder Bandpasssignal')
        d += (a10 := schemdraw.elements.lines.Annotate()).at(l15.center).to((l13.center[0]-4,l13.center[1])).color('#00B9FF').label('')
        # Demodulator
        d += (demod := dsp.Ic(pins=[dsp.IcPin(side='R'), dsp.IcPin(side='R'), dsp.IcPin(side='L'), dsp.IcPin(side='T')],
                            size=(2,2), leadlen=0.0).anchor('inL1').label('De-\nmodulator')).at(l15.end).linestyle('--').fill(nue2Color)
        d += (l16 := schemdraw.elements.lines.Line()).at(demod.inR1).left().length(1)
        d += (l17 := schemdraw.elements.lines.Line()).at(demod.inR2).left().length(1).linestyle('--')
        d += (a11 := schemdraw.elements.lines.Annotate()).at(l16.center).to((l16.center[0],l16.center[1]+1)).color(annColor).label('(komplexes)\nanaloges\nSignal')    
        # ADC
        d += (adc := dsp.Ic(pins=[dsp.IcPin(side='R'), dsp.IcPin(side='R'), dsp.IcPin(side='L'), dsp.IcPin(side='L'), dsp.IcPin(side='T')],
                            size=(2,2), leadlen=0.0).anchor('inL2').label('Analog-\nDigital-\nKonverter')).at(l17.end).fill(nue1Color)
        d += (l18 := schemdraw.elements.lines.Line()).at(adc.inR1).left().length(1)
        d += (l19 := schemdraw.elements.lines.Line()).at(adc.inR2).left().length(1).linestyle('--')
        d += (a12 := schemdraw.elements.lines.Annotate()).at(l18.center).to((l18.center[0],l18.center[1]+1)).color(annColor).label('(komplexes)\ndigitales\nSignal')
        d += (arr2 := schemdraw.elements.lines.Arc2(arrow='<->',k=-0.5)).to(demod.inT1).at(adc.inT1).linestyle('--')
        # matched filter
        d += (mf := dsp.Ic(pins=[dsp.IcPin(side='R'), dsp.IcPin(side='R'), dsp.IcPin(side='L'), dsp.IcPin(side='L')],
                            size=(2,2), leadlen=0.0).anchor('inL2').label('(matched) \n filter \n/ Entzerrer/\n dsp')).at(l19.end).fill(nue1Color)
        d += (l20 := schemdraw.elements.lines.Line()).at(mf.inR1).left().length(1)
        d += (l21 := schemdraw.elements.lines.Line()).at(mf.inR2).left().length(1).linestyle('--')
        d += (a13 := schemdraw.elements.lines.Annotate()).at(l20.center).to((l20.center[0],l20.center[1]+1)).color(annColor).label('(komplexes)\ndigitales\nSignal')
        # demapper
        d += (dm := dsp.Ic(pins=[dsp.IcPin(side='R'), dsp.IcPin(side='L'), dsp.IcPin(side='L')],
                            size=(2,2), leadlen=0.0).anchor('inL2').label('De-\nmapper / \n Entscheider')).at(l21.end).fill(nue1Color)
        d += (l22 := schemdraw.elements.lines.Line()).at(dm.inR1).left().length(1)    
        d += (a14 := schemdraw.elements.lines.Annotate()).at(l22.center).to((l22.center[0],l22.center[1]+1)).color(annColor).label('Bits')
        # line decoding
        d += (ld := dsp.Box(w=2,h=2)).label('Leitungs-\nde-\ncodierung').at(l22.end).linestyle('--').fill(nue1Color)
        d += (l23 := schemdraw.elements.lines.Line()).at(ld.W).left().length(1)    
        d += (a15 := schemdraw.elements.lines.Annotate()).at(l23.center).to((l23.center[0],l23.center[1]+1)).color(annColor).label('Bits').fill(nue1Color)
        # channel decoding
        d += (cd := dsp.Box(w=2,h=2)).label('Kanal-\nde-\ncodierung').at(l23.end).linestyle('--').fill(nue2Color)
        d += (l24 := schemdraw.elements.lines.Line()).at(cd.W).left().length(1)    
        d += (a16 := schemdraw.elements.lines.Annotate()).at(l24.center).to((l24.center[0],l24.center[1]+1)).color(annColor).label('Bits')
        # source decoding
        d += (sd := dsp.Box(w=2,h=2)).label('Quellen-\nde-\ncodierung').at(l24.end).linestyle('--').fill(nue1Color)
        d += (l25 := schemdraw.elements.lines.Line()).at(sd.W).left().length(1)    
        d += (a17 := schemdraw.elements.lines.Annotate()).at(l25.center).to((l25.center[0],l25.center[1]+1)).color(annColor).label('Bits')
        # sink
        d += (dsk := dsp.Box(w=2,h=2)).label('Digitale\nDaten-\nsenke').at(l25.end).fill(nue1Color)
        
        return d
        #d.save(r'.\images\nue1_overview.svg')

def gen_time_signal(sr=1e3, T=1):
    t = skc.utils.create_time_axis(sample_rate=sr, n_samples=int(sr/T))
    f1 = 5.
    f2 = 2.
    x = (np.sin(2*np.pi*f1*t) + 0.9*np.cos(2*np.pi*f2*t)) * np.blackman(t.size)   
    x -= np.mean(x)
    x /= np.max(np.abs(x))
    return x

def gen_generic_freq_signal(n=1e3):
    x = ssignal.windows.general_gaussian(n, 3, n/4, sym=True) * np.exp(-1j*np.arange(start=-int(n/2),stop=int(n/2))/n)
    return x

def plot_as_arrows(axes, xlabel=('',1.0,0.0), ylabel=('',0.0,1.0)):
    # axes := matplotlib.axes.Axes
    # x/ylabel=('string to be printed', xcoord in axes fractions from bottom left, ycoord in axes fractions from bottom left)

    # remove ticks and grid
    axes.set(xticks=[],yticks=[])
    plt.grid(visible=False)
    # set left / bottom spine properties 
    axes.spines['left'].set(color='k',position=('data',0),visible=True,ls='-',lw=1.5,fill=True,zorder=0)
    axes.spines['bottom'].set(color='k',position=('data',0),visible=True,ls='-',lw=1.5,fill=True, zorder=0)
    # plot arrow at the end of the left / bottom spine https://matplotlib.org/3.3.4/gallery/recipes/centered_spines_with_arrows.html
    axes.plot(0, 1, "^k", transform=axes.get_xaxis_transform(), clip_on=False)    
    axes.plot(1, 0, ">k", transform=axes.get_yaxis_transform(), clip_on=False)    
    # annotate left / bottom spine
    axes.annotate(ylabel[0], xy=(ylabel[1], ylabel[2]), xycoords='axes fraction')
    axes.annotate(xlabel[0], xy=(xlabel[1], xlabel[2]), xycoords='axes fraction')
    # remove top and right spine
    axes.spines["top"].set(visible=False)
    axes.spines["right"].set(visible=False)
    
    